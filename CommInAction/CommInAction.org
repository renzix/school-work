* Day 1
** Group Project Ideas
*** Soulth African Culture
    Technology
    Economy
    Day-to-day
*** European Culture
    Day-to-day
    Clothing
    
*** Buddist Culture
    Way of life
    Day to day
    
** Lecture
*** Communication
    Communication is a Systemic process in which people exchange
    symbools to create and interpret meanings
   
    Process
    System
    Symbols
    Meaning -> Content: Literal, Relational: Implied
** Modals of Communication
*** Communication as Trasmission
    Source sends a message through a channel to a reciever. Noise exists
    which can prevent or distort the transmission.
*** Communication as a Interaction
    Two way process of actions.
    Adds feedback to the previous modal.
*** Communication as Transaction
    Simultaneously sending and receiving messages in a relationship to one
    another.

** Communication Competence
   Refers to the ability to communicate in a effective yet socially
   appropriate manner.
   Skill, Knowledge, and Motivation.
* Day 2
** Types of Informtive Speeches
   About objects and object is visable, tangible, and stable in
   form. (physical objects, Places, Animals, Humans...)
   
   Speech about a process. Series of actions that lead to specific
   result or product.
   
   Events. Something that already happend
   
   Concepts. Beliefs, Theories, Ideas, Principles, etc. Concepts are abstract.

** Informative Guidlines
   Dont overestimate/be too technical.
   Relate the subject
   Avoid abtraction
   Personalize your ideas
** Persuasive Speeches
   Question of fact - Whether something is ture or false.
   
   Question of value - Question about morality, rightness, worth and
   so forth of a idea or action.
   
   Question of policy - question about whether a specifc course of
   action should be taken or not taken. There must be some kind of
   problem. Explain how your plan is supposed to fix the
   need. Convince that the plan is actually gonna work.

** Organize
   Problem cause solution Model
   -Intro
   -Body
   --Problem
   --Cause
   --Solution
   Monroe Motivated Sequence Pattern
   -Attention
   -Need (show there is a need)
   -Satisfaction (provide a solution and show how it will work)
   -Visualization (help them visualize the world with your plan)
   -Action (Tell them exactly how you want them to do it)
   
** Stuff notes
   Misconceptions
   Social Media
   USERNAME: @XYZ53586307
   PASSWORD: commclass1100
* [2019-03-19 Tue] 
  10 verbal, 10 nonverbal
** Language
   Definition: A system of symbols (words) structured by rules (grammar) 
   and patterns (syntax) common to a community of people.
   Verbal communication refers to the spoken or written words. 
** Semantics
   Words have 2 levels of meaning, Denotative meaning: primary and literal
   and connotative meaning: the secondary meaning of a word (person subjective)
   Semantic Triangle:
   - Referent: Thing thats being percieved
   - Symbol: Word that representing the Referent
   - Reference: The association tied to the referent; how you would describe
   the referent
** Principals of verbal communication
   Words 
   are arbitrary
   are culturally bound
   vary in their abtraction
** Deception
   Deception is deliberate/intentional
   Your aim is to mislead and believe something that you know is not true/false
   This excludes honest mistakes and self deception
   In order to decieve you just reverse Grice's work
   Falsification (quality)
   Ommision (quantity) the most popular as you just leave out information
   Evasion (relevance)
   Equivocation (manner) you are being super unclear/misleading
*** Grice's work
    Cooperation Principle: Make your contribution appropriate to the conversation
    Four Maxims
    - Quality: Dont say things that are false
    - Quantity: Provide enough info but not anymore then you need
    - Relevance: Be relevant to the conversation
    - Manner: Be clear and don't be obscure
** Verbal Agression
   Definition: Attacking a person's self-concept. Instead of a arguement
** Verbal Expressions of Intimacy
   Self Disclosure
   Verbal Immediacy (we/us/our)
   Assurances (emphasis there is a relationship)
   Mundane everyday talk
   Personal idioms (kinda inside jokes but not really)
** Power Verbally
   Powerless speech
   - Tag questions
   - Qualifiers
   - Disclaimers
   - Hesitation forms
** Functions of Nonverbal Behavior
   Repeating
   Conflicting
   Complementing (interpreted)
   Substituting (no verbal comm)
   Accenting is for emphasis
** Channel Reliance
   Children rely more on verbal cues than adults do
   Adults rely more on nonverbal cues than verbal cues
   When they conflict adults tend to rely more on nonverbal messages
   to determine meaning. 
** Nonverbal Communication Cues
   Facial Expressions
   - Six basic facial expressions: Happiness, Sadness, Anger, Fear,
     Disgust, Suprise.
   - Display rules: Different cultures have different rules about
     emotions in different situations.
   Eye Contact
   Kinesics: any movement we make with our bodies.
   - Gestures: Emblems(universally understood), Illustrators(draws a
     picture), and regulators.
   Haptics: touch
   Vocalics: Vocal Qualities
   Proxemics: Interpersonal distance
   Chronemics: Use of time
   Physical Appearance
   Artifacts
   The Environment
** Nonverbal Skills
   Expressivity (encoding skill): Abillity to send readable nonverbal
   cues.
   Sensitivity (decoding skill): The ability to interpret nonverbal
   cues accurately.
   Control: Ability to display what is appropriate and suppress what
   is inappriopriate.
** Communication Accommodation Theory
   Basically different communications per different group
   Short answer Question below
*** Convergence
    Definition Adapting your comm behavior so that it is more similar to
    your partner's behavior. Typically convergence is a good thing and
    make favorable evaluations with the exception of making fun of
    them. 
    - Reflects a desire to be socially approved
    - Can be used to facilitate understanding
*** Divergence
    Adapt your behavior so it becomes less similar to your partner or
    group. Is generally perceived negatively.
    - Can be used to show disdain for others
    - Shows valued group identity
* [2019-03-26 Tue] 
** Part 1
*** Componenets of listening
    1) Hearing
    2) Attending
    3) Understanding
    4) Responding (feedback)
    5) Recalling (you have to be able to recall it later in order to
       actually listen)
*** Types of Listening
    1) Appreciative Listening
    2) Comprehensive Listening
    3) Critical Listening (evaluate it)
    4) Empathic Listening (emotional support)
*** Barriers to listening
    1) Preoccupation (already doing something)
    2) Criticizing the speaker
    3) Prejudgments
    4) Information rate (we can proccess about 600 words per minute)
    5) Message Overload
    6) Message Complexity
    7) Environmental Distractions
*** Forms of ineffective listening (short answer)
    Pseudolistening - Pretending to listen
    Monopolizing - Everything is about themselves
    Selective listening - Only listening to parts you find interesting
    Defensive listening - When you percieve a attack when nothing was intended
    Ambushing - Listening really carefully to ambush you with your own words
    Literal listening - Actual words but not the real meaning
*** Guidelines for effective listening
    Make a conscious decision to listen
    Eliminate and resist external and internal distractions
    Focus on the message, not the speaker
    Ask a question for clarification
    prejudgment is a problem so suspend judgment until you hear the
    entire message
** Part 2
*** Perception Process
    Selection: Choosing what stimuli we notice. Attention, Exposure,
    Perception, Memory.
    Organization: we arrange our perceptions in meaningful
    ways. Prototype(clear representitive of a single catagory),
    Stereotypes, Personal constructs, and Scripts
    Interpretation: Create explaination for what we observe and
    experience.
*** Self concept
    We develop perceptions of who we are through our communication
    with others.
    - Self Image (how you think others value you)
    - Self Esteem (how much you value yourself)
    Influenced by others
    - Particular others
    - Generalized others
   
* [2019-04-02 Tue] Interpersonal Communication
** Types of Attraction
   Pick 2 name them and explain them
   1) Task Attraction: They are smart, competent and stuff.
   2) Physical Attraction
   3) Sexual Attraction: Not always the same as physical attraction
   4) Social Attraction: Good friend, fun person to hang out with.
   5) Relational Attraction: Want to have a relationship, subset of
      social attraction
** Factors Influencing Attraction
   1) Proximity: Physical closeness
      * Mere exposure hypothesis, Repeated exposure usually increases
	our liking for that person.
   2) Physical Attractiveness
      * Symmetry
      * Waist to hip ratio
      * Average features
      * Matching hypothesis: matching similarly attractive partners
   3) Similarity
   4) Reciprocity
   5) Barriers to Romace
      * Romeo and Julliet Effect
      * The "closing time effect"
      * The "Hard to get" pehnomenon
   6) Misattribution of arousal: Mistakes were made
** Social Penetration Theory
   As our relationships develop our self disclosure increases in both
   breadth and depth.
   - Breadth: Number of topics we discuss
   - Depth: The personal significance of the topics
** Knapps stages of coming together and coming apart
   SEE WORKSHEET
   Also this is a linear path
** Relationships Turning Points
   - Relationships take a nonlinear path
** Dialectial Perspective
   - Relationships never are stable and are constantly changing
   - Dialectical tension: A push/pull between 2 important but
     contradictory needs.
   - Three basic dialects
     + Autonomy vs Connection (freedom/connection)
     + Novelty vs Predictability
     + Openness vs Closedness
** Responses
   - Selection: Choose to do one a say fuck the other one which people
     may like
   - Seperation: Trying to satisfy both needs
   - Neutralization: 
   - Reframing: Changing how you look so that both of the needs are
     not always oppisite. Autonomy means we are so connected of course
     we dont need to be connected at all times
** Relationship Maintenance
   Relationship involves keeping a relationship
   - In existence
   - In a specified state or condition
   - In repair
   - In satisfactory condition
   What can we do
   - Strategic Maintenance Behaviors: Specifically intentional to help
     the relationship
   - Routine Maintenance Behaviors: Not intentional but still helps
     the relationship
     
* [2019-04-09 Tue] Pauls Dimension for comparing cultures
** High Context vs Low Context
   High Context is implicit rather than explicit
   Communicators get more info fromnonverbal situational cues and less
   information from words
** Contact vs Noncontact
   More pysical contact direct body orientation, direct gaze and
   stuff. 
   Noncontact greater interactions less immediacy
** Other Cultral Dimensions
   Masculine or Feminine
   Msculine values ambition competetiveness and strength. Value on
   material objects and power.
   Feminine culture is more about affection compassion and emotions.
   Power Distance: How equally wealth is distributed in a certain
   culture or social context.
   Uncertaincy avoidance: Some cultures handle ambiguety really well
   while others really don't.
   Multiple social communities can coexist in a single
   culture. Multiple groups exist per person.
** Responses to Cultural Diversity
   - Resistance: Denies the value of these cultres
   - Assimilation: Change your culture
   - Tolerance: Doesnt accept it but tolerates it
   - Understanding: Understand differences. No one culture is better
     than another
   - Respect: Respect all cultures and see value in other culture
   - Participation: Incorperate aspects of other cultures in your own life
** Guidelines for Competent Communication(PROBABLY GOOD SHORT ANSWER STUFF)
   - Be aware of cultral differences
   - Avoid overgeneralization
   - Engage in person centered communication
   - Avoid Ethnocentrism (your culture is the best)
** Group and teams
   A Group is defined by...
   - Three or more interdependant people
   - interaction over time
   - Follow a shared set of rules for conduct
   - Trying to reach a common goal
   A Team is fedined by...
   - Same characteristics of a group
   - Each person beings unique resources to a common project
   - Tends to have a stronger sense of identity then a group
** Tuckman stages of group development
   1) Forming: Forms the group
   2) Storming: We are starting to express our differences and opinions
   3) Norming: Set of implicit/explicit rules about how to accomplish
      the goals at hand
   4) Performing: Things are going smoothly and the group is focussed
      and understand what they need to do in order to accomplish the
      goal at hand
   5) Adjourning: Mission accomplished. Group disbands
** Features of small groups
   - Cohesion
     + Groupthink, saying yes to a group
   - Group Size
     + Ideally 5-7
     + More people means less contribution then less cohesion
   - Power Structure
     + Distrubuted power structure(equal amount of power)
     + Hierchial power structure(one or 2 members have the most)
   - Interaction patterns
     + Centralized patterns
     + Decentralized patterns
   - Group norms
** Roles
   Group members have consistent ways of communicating with others in
   the group.
   Expectations of others and yourself define what the roles are.
   *Types*
   - Task Role: Roles that help the group achieve its goals
   - Social Role: Helps then maintain the relationships between group members
   - Individual Role: Roles that focus attention on other people not
     the project or goal
** Functional Perspective of a small group communication
   1) Develop correct understanding of the issue
   2) Find out options
   3) 
   4) Examine the alternatives
** Small Group Conflict
   Four types
   * Substantive (centers on crital evaluations)
   * Affective (disrupts the group)
   * Procedural (right order of doing the task)
** Handling conflicts
   - Stick to the issue
   - Talk to eachother not everyone else
   - Be respect of others
** SPEECH
   Use the steps of that dude

* Day 2
  Difference between psychology and ethics is that pyschology is WHY
  while ethics is WHAT is right.
** Moral vs Ethics (arguable point, can be individual or apply to everyone)
   They are arguably 2 different thing
   Moral is what you personally believe (only applies to you)
   Moral = M = Me
   Ethical is beliefs or values that should apply to EVERYONE
   Ethical = E = Everyone
** HW
   Journal Audio/Video recording, 100 or 0
   @DUE: 1/30/19

* Day 3
  Instrinsic vs Instrumental
** Instrinsic
   Valuable for its own sake
   ex. Love - Can be both
** Instrumental
   Valuable because it allow you to get something else
   ex: Love because it makes you feel good
   ex: Money because you can use it to get other stuff -Only intrumental
* [2019-02-06 Wed]
** Why do we need Ethics
   1. Keep society intact
   2. Counteract human suffering
   3. Help others at what they do
   4. Give order to conflict and stuff
   5. reward/punish good/bad behavior
** House
   Position

   Reasons
   Value/Core
   Evidence: Supports
   Example: Illutrates

   Assumptions

   Challenges (How can someone ever disagree)
   Response to the challenges
** Q2 House
   Assumptions:
   We have to resolve conflict
   Fairness is good
   We can implement a fair system
   There is a ultimate form of justice
   Justice is a fair decision which results in punishment or reward
  
   Reasons:
   Justice is needed in order to keep piece
   Cultures without 2 fall into anarchy
   Rodney king riots (whole city burned down for it)

   Challenges:
   1. Can humans achieve true Justice? :Assumption
   2. Does a Single true justice actually exist? :Assumption
   3. Why does conflict need to be resolved
   
   Responses:
   
   2. General consensus around the culture envolved with it.
* [2019-02-13 Wed]
  Ethics for Authority
  Pro's
  People should believe in others that are smarter then themselves.
  Con's
  People are rarely completely correct.
  Small groups are not always uniform in the way they think.
* [2019-02-27 Wed]
  Deontology: Any ethical theory which has a clearly defined rule 
** Golden rule
   Do what you want for yourself onto other
** Kant: 2 ways of respect
   - Universalize action. If everyone does something would it undermine
     the purpose of doing the action. ie survival of the fittest
   - Respect everyone
** Virtue Ethics
   All problems of outcome/intention are based on a list of virtues
** Care Ethics
   Intentions are about care.
   The outcome is more towards loved ones or people you care about.
   Increases the likely hood of happiness more than most. Very
   easy/natural/faster to follow.
* [2019-03-01 Fri]
  Types of choices
** Restricted Choice
   Shows us what we already value
   Limited options
   Able to find a seek advice from others
   Possibly subconscious
** Ultimate Choice
   Changes our ethics based on the actual situation
   Can cause a actual change in your values
   <<<<<<< Updated upstream
* [2019-03-22 Fri] 
  Ethical Framworks
  Not the same as a ethical theory because you can believe whatever
  you want under it. Its just rules we can judge based on the ethical
  theories we have.
* [2019-03-27 Wed] 
  Judgement is internal or external AND positive or negitive
  On the spectrum there is
  Subjectivism---------Realativism----------Pluralism---------Obejectivism

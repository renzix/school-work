#!/bin/sh

# Checks if clean was passed 
if [ "$#" -ne 0 ]; then
    case "$1" in
        "clean")
            printf "Cleaning\n"
            rm -fv out/*
            rm -fv obj/*
            return 0;
            ;;
        *)
            # Checks if numbers 1-5 were passed and run those interactively
            # case statements in bash are awful. A match syntax would be really
            # nice for pattern recognition.
            if [ "$1" -ge 1 -a "$1" -le 5 ]; then
                javac -d obj/ src/ExerciseIIPart${1}.java
                java -classpath obj/ ExerciseIIPart${1}
                return $?
            else
                return 1;
            fi
            ;;
    esac
fi

# Builds the files and sends them to out/
printf "Starting build\n"
for file in src/*.java; do
    printf "Build for $file\n"
    javac -d obj/ $file
    java -classpath obj/ $(basename -s .java $file) < input > out/$(basename -s .java $file).output.txt
    cp $file out/$(basename -s .java $file).source.txt
done
#2 more files which need to be in it
cp -v build.sh out/build.sh.txt
cp -v input out/input.txt
tree . > out/tree.txt

# Now test if they are the same output
printf "Starting Tests\n"
basefile="out/ExerciseIIPart1.output.txt"
for file in out/*.output.txt; do
    diff $file $basefile > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        printf "$file is the same as $basefile\n"
    else
        printf "$file is different from $basefile\n"
        diff $file $basefile
        return 1;
    fi
done

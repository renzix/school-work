/* NAME: Daniel DeBruno 
 * ASSIGNMENT: ExerciseIIPart1*/
import java.util.Scanner;

class ExerciseIIPart1 {
    public static void main(String[] tArgs) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Please enter your first name: ");
        String firstName = sc.nextLine();
        
        System.out.print("Please enter your last name: ");
        String lastName = sc.nextLine();
        
        System.out.print("Please enter your id: ");
        int id = sc.nextInt();
        
        System.out.print("Please enter your pay rate: ");
        double payRate = sc.nextDouble();

        System.out.print("Please enter your number of hours: ");
        int hoursWorked = sc.nextInt();
        sc.close();
        
        double gross = payRate * hoursWorked;

        double taxDeduction = gross*0.20;
        double netPay = gross-taxDeduction;
        System.out.println("======================================");
        System.out.printf("Name:          \t%s, %s\n", lastName, firstName);
        System.out.printf("Hours:         \t%d\n", hoursWorked);
        System.out.printf("Pay Rate:      \t$%.2f\n", payRate);
        System.out.printf("Gross Pay:     \t$%.2f\n", gross);
        System.out.printf("Tax Deduction: \t$%.2f\n", taxDeduction);
        System.out.printf("Net Pay:       \t$%.2f\n", netPay);
        System.out.println("======================================");
    }
}

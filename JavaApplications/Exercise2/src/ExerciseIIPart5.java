/* NAME: Daniel DeBruno 
 * ASSIGNMENT: ExerciseIIPart5*/
import java.util.Scanner;

class ExerciseIIPart5 {
    static double tax;
    static double netPay;
    public static void main(String[] tArgs) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Please enter your first name: ");
        String firstName = sc.nextLine();
        
        System.out.print("Please enter your last name: ");
        String lastName = sc.nextLine();
        
        System.out.print("Please enter your id: ");
        int id = sc.nextInt();
        
        System.out.print("Please enter your pay rate: ");
        double payRate = sc.nextDouble();

        System.out.print("Please enter your number of hours: ");
        int hoursWorked = sc.nextInt();
        sc.close();
        
        double gross = payRate * hoursWorked;

        TaxNetPay tnp = new TaxNetPay();
        tax = tnp.computeTax(gross);
        netPay = tnp.computeNetPay(gross);
        System.out.println("======================================");
        System.out.printf("Name:          \t%s, %s\n", lastName, firstName);
        System.out.printf("Hours:         \t%d\n", hoursWorked);
        System.out.printf("Pay Rate:      \t$%.2f\n", payRate);
        System.out.printf("Gross Pay:     \t$%.2f\n", gross);
        System.out.printf("Tax Deduction: \t$%.2f\n", tax);
        System.out.printf("Net Pay:       \t$%.2f\n", netPay);
        System.out.println("======================================");
    }
}

class TaxNetPay {
    public double computeNetPay(double gross) {
        return gross - this.computeTax(gross);
    }
    public double computeTax(double gross) {
        return gross * 0.20;
    }
}

////////////////////////////////
// NAME: Daniel DeBruno       //
// DATE: 9/25                 //
// EDITOR: emacs(Best editor) //
////////////////////////////////


import java.util.Scanner;


/* I am unsure where you want these functions but i think this is
 * right acording to the common sense. If you really want a seperate class
 * for printTestResults and getLetterGrade then you would have to have
 * both classes own a instance of each other which makes no sense. Also
 * instance methods require a class as all methods that are called by 
 * main to be static...

 * the class (static) method double [ ] readTestScores( int size ),
 * The instance method char getLetterGrade(double score), The class
 * (static) method void printComment(char grade), and The instance
 * method void printTestResults(double [ ] testList).
 */

public class ExerciseI2 {

    private static Scanner scan; 
    public static void main(String[] tArgs) {
        scan = new Scanner(System.in);
        double[] testList = readTestScores(4);
        printTestResults(testList);
    }

    private static void printComment(char grade) {
        if (grade == 'A')
            System.out.print("very good");
        else if (grade == 'B')
            System.out.print("good");
        else if (grade == 'C')
            System.out.print("satisfactory");
        else if (grade == 'D')
            System.out.print("need improvement");
        else
            System.out.print("poor");
    }

    private static void printTestResults(double[] testList) {
        System.out.printf("%-20s%-20s%-20s\n", "Test Score", "Letter Grade", "Comment");
        char grade;
        for (int i = 0; i < testList.length; i++) {
            grade = getLetterGrade(testList[i]);
            System.out.printf("%-20s%-20s", testList[i], grade);
            printComment(grade);
            System.out.println();
        }
    }

    
    private static char getLetterGrade(double score) {
        if (score >= 90)
            return 'A';
        else if (score >= 80)
            return 'B';
        else if (score >= 70)
            return 'C';
        else if (score >= 60)
            return 'D';
        else
            return 'F';
    }
    
    // @NOTE(DeBruno): I could make another variable so that its 0->size but
    // thats boring and this saves a variable. Also its not in the
    // requirements to preserve the input order.
    private static double[] readTestScores(int size) {
        double testList[] = new double[size--];
        for (;size>=0;size--) {
            System.out.print("Enter your grade: ");
            testList[size] = scan.nextDouble();
        }
        System.out.println();
        return testList;
    }

}

////////////////////////////////
// NAME: Daniel DeBruno       //
// DATE: 9/25                 //
// EDITOR: emacs(Best editor) //
////////////////////////////////

import java.util.Scanner;

public class ExerciseI4 {

    public static Scanner scan; 
    public static void main(String[] tArgs) {
        scan = new Scanner(System.in);
        ExerciseI4Methods ei3m = new ExerciseI4Methods();
        double[] testList = ei3m.readTestScores(4);
        ei3m.printTestResults(testList);
    }
}

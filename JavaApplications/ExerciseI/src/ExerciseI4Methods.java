////////////////////////////////
// NAME: Daniel DeBruno       //
// DATE: 9/25                 //
// EDITOR: emacs(Best editor) //
////////////////////////////////


public class ExerciseI4Methods {
    static public void printComment(char grade) {
        if (grade == 'A')
            System.out.print("very good");
        else if (grade == 'B')
            System.out.print("good");
        else if (grade == 'C')
            System.out.print("satisfactory");
        else if (grade == 'D')
            System.out.print("need improvement");
        else
            System.out.print("poor");
    }

    public void printTestResults(double[] testList) {
        System.out.printf("%-20s%-20s%-20s\n", "Test Score", "Letter Grade", "Comment");
        char grade;
        for (int i = 0; i < testList.length; i++) {
            grade = getLetterGrade(testList[i]);
            System.out.printf("%-20s%-20s", testList[i], grade);
            printComment(grade);
            System.out.println();
        }
    }

    
    public char getLetterGrade(double score) {
        if (score >= 90)
            return 'A';
        else if (score >= 80)
            return 'B';
        else if (score >= 70)
            return 'C';
        else if (score >= 60)
            return 'D';
        else
            return 'F';
    }    

    // @NOTE(DeBruno): I could make another variable so that its 0->size but
    // thats boring and this saves a variable. Also its not in the
    // requirements to preserve the input order.
    static public double[] readTestScores(int size) {
        double testList[] = new double[size--];
        for (;size>=0;size--) { // 
            System.out.print("Enter your grade: ");
            testList[size] = ExerciseI4.scan.nextDouble();
        }
        System.out.println();
        return testList;
    }

}

#!/usr/bin/env dash

################
# Build Script #
################

set -e

cp -v build.sh out/build.txt

# javac -d obj src/O1/*
# java -classpath obj O1.ExerciseO1 > out/ExerciseO1.output.txt
# cp -v src/O1/ExerciseO1.java out/ExerciseO1.source.txt
# cp -v src/O1/demo.java out/demoO1.source.txt

# javac -d obj src/O2/*
# java -classpath obj O2.ExerciseO2 > out/ExerciseO2.output.txt
# cp -v src/O2/ExerciseO2.java out/ExerciseO2.source.txt
# cp -v src/O2/demo.java out/demoO2.source.txt

# javac -d obj src/O3/*
# java -classpath obj O3.ExerciseO3 < input_O3 > out/ExerciseO3.output.txt || echo "ExerciseO3 failed exit code: $?"
# cp -v src/O3/ExerciseO3.java out/ExerciseO3.source.txt
# cp -v src/O3/Date.java out/DateO3.source.txt
# cp -v input_O3 out/input_O3.txt

# javac -d obj src/O4/*
# java -classpath obj O4.ExerciseO4 < input_O4 > out/ExerciseO4.output.txt || echo "ExerciseO4 failed exit code: $?"
# cp -v src/O4/ExerciseO4.java out/ExerciseO4.source.txt
# cp -v src/O4/Date.java out/DateO4.source.txt
# cp -v src/O4/Employee.java out/EmployeeO4.source.txt
# cp -v input_O4 out/input_O4.txt

# javac -d obj src/O5/*
# java -classpath obj O5.ExerciseO5 < input_O5 > out/ExerciseO5.output.txt
# cp -v src/O5/ExerciseO5.java out/ExerciseO5.source.txt
# cp -v src/O5/Date.java out/DateO5.source.txt
# cp -v src/O5/Employee.java out/EmployeeO5.source.txt
# cp -v input_O5 out/input_O5.txt

# javac -d obj src/O6/*
# java -classpath obj O6.ExerciseO6 < input_O6 > out/ExerciseO6.output.txt
# cp -v src/O6/ExerciseO6.java out/ExerciseO6.source.txt
# cp -v src/O6/Score1.java out/Score1.txt
# cp -v input_O6 out/input_O6.txt

# javac -d obj src/O7/*
# java -classpath obj O7.ExerciseO7 < input_O7 > out/ExerciseO7.output.txt
# cp -v src/O7/ExerciseO7.java out/ExerciseO7.source.txt
# cp -v src/O7/Score2.java out/Score2.txt
# cp -v input_O7 out/input_O7.txt

# javac -d obj src/O8/*
# java -classpath obj O8.ExerciseO8 < input_O8 > out/ExerciseO8.output.txt
# cp -v src/O8/Date.java out/DateO8.source.txt
# cp -v src/O8/Employee.java out/EmployeeO8.source.txt
# cp -v src/O8/BonusEmployee.java out/BonusEmployeeO8.source.txt
# cp -v src/O8/ExerciseO8.java out/ExerciseO8.source.txt
# cp -v input_O8 out/input_O8.txt

# javac -d obj src/O9/*
# java -classpath obj O9.ExerciseO9 < input_O9 > out/ExerciseO9.output.txt
# cp -v src/O9/Date.java out/DateO9.source.txt
# cp -v src/O9/Employee.java out/EmployeeO9.source.txt
# cp -v src/O9/BonusEmployee.java out/BonusEmployeeO9.source.txt
# cp -v src/O9/ExerciseO9.java out/ExerciseO9.source.txt
# cp -v input_O9 out/input_O9.txt

# javac -d obj src/O11/*
# java -classpath obj O11.ExerciseO11 > out/ExerciseO11.output.txt
# cp -v src/O11/Date.java out/DateO11.source.txt
# cp -v src/O11/Employee.java out/EmployeeO11.source.txt
# cp -v src/O11/BonusEmployee.java out/BonusEmployeeO11.source.txt
# cp -v src/O11/ExerciseO11.java out/ExerciseO11.source.txt

# javac -d obj src/O12/*
# java -classpath obj O12.ExerciseO12 > out/ExerciseO12.output.txt
# cp -v src/O12/Tile.java out/TileO12.txt
# cp -v src/O12/RectangleTile.java out/RectangleTileO12.txt
# cp -v src/O12/TriangleTile.java out/TriangleTileO12.txt
# cp -v src/O12/ExerciseO12.java out/ExerciseO12.source.txt

javac -d obj src/O13/*
java -classpath obj O13.ExerciseO13 > out/ExerciseO13.output.txt
cp -v src/O13/Payroll.java out/PayrollO13.txt
cp -v src/O13/HourlyPay.java out/HourlyPayO13.txt
cp -v src/O13/ExerciseO13.java out/ExerciseO13.source.txt

javac -d obj src/O14/*
java -classpath obj O14.ExerciseO14 > out/ExerciseO14.output.txt
cp -v src/O14/ExerciseO14.java out/ExerciseO14.source.txt

javac -d obj src/O15/*
java -classpath obj O15.ExerciseO15 > out/ExerciseO15.output.txt
cp -v src/O15/ExerciseO15.java out/ExerciseO15.source.txt

javac -d obj src/O16/*
java -classpath obj O16.ExerciseO16A > out/ExerciseO16A.output.txt
java -classpath obj O16.ExerciseO16B > out/ExerciseO16B.output.txt
cp -v src/O16/ExerciseO16A.java out/ExerciseO16A.source.txt
cp -v src/O16/ExerciseO16B.java out/ExerciseO16B.source.txt
cp -v src/O16/ChangeMaker.java out/ChangeMakerO16.txt
cp -v src/O16/Expression.java out/ExpressionO16.txt

tree . > out/tree.txt

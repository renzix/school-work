package O2;

public class ExerciseO2 {
    public static void main(String[] tArgs) {
        Demo obj1 = new Demo();
        obj1.setValues(5,7);
        Demo.decrDemo(obj1);
        System.out.printf("val1: %.3f\nval2: %.3f\n", obj1.getValue1(), obj1.getValue2());
    }
}

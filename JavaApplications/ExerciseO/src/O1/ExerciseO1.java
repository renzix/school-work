package O1;

public class ExerciseO1 {
    public static void main(String[] tArgs) {
        Demo item = new Demo();
        item.setValues(4,9);
        System.out.printf("The average of item is: %.3f\n", item.getAverage());
        Demo obj1 = new Demo();
        obj1.setValues(5,7);
        Demo obj2 = new Demo();
        obj1.setValues(14,9);
        Demo objR = addDemo(obj1,obj2);
        incDemo(obj1);
        System.out.printf("objR: %.3f,%.3f\nobj1: %.3f,%.3f\n", objR.getValue1(), objR.getValue2(), obj1.getValue1(), obj1.getValue2()); // Im assuming you dont want the default toString()
    }

    static Demo addDemo(Demo d1, Demo d2) {
        Demo ret = new Demo();
        ret.setValues(d1.getValue1()+d2.getValue1(),d1.getValue2()+d2.getValue2());
        return ret;
    }

    // The paper says to create a incDemo2 but it says to use incDemo
    // "Increment the value of each instance variable of object obj1 by 5 by calling incrDemo(  )."
    // "Static method incrDemo2 that receives as argument an object of the class Demo and increments the value of each of its member variables by 5."
    static void incDemo(Demo d) {
        d.setValues(d.getValue1()+5,d.getValue2()+5);
    }
    
}

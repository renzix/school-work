package O11;

class ExerciseO11 {
    public static void main(String[] tArgs) {
        Date d = new Date(1,2,1998);
        System.out.println(d);
        Employee e = new Employee("Dan", "DeB", 10, new Date(5,11,2000), new Date(27,2,2011), 1000);
        System.out.println(e);
        BonusEmployee be = new BonusEmployee("Tom", "DeB", 13, new Date(2,4,1997), new Date(25,3,2016), 1000, 200);
        System.out.println(be);
        Date today = new Date(7,10,2019);
        System.out.printf("Today is class type: %s\n", today.getClass());
    }
}

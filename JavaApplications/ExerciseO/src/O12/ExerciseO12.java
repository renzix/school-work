package O12;

// BTW functionname(void) isnt valid java...

class ExerciseO12 {
    public static void main(String[] tArgs) {
        Tile tile1 = new RectangleTile(3.0,5.0,4.0);
        tile1.print();
        Tile tile2 = new TriangleTile(2.0,6.0,8.0);
        tile2.print();
    }
}

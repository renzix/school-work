package O12;

class RectangleTile extends Tile {
    private double length;
    private double width;

    // Again i guess im just copying from the document
    // I dont understand why we would even have a empty
    // contructor...
    public RectangleTile() {
        length = 0.5;
        width = 1.0;
    }

    public RectangleTile(double length, double width, double unitPrice) {
        super(unitPrice);
        this.length = length;
        this.width = width;
    }

    @Override
    public double computeArea() {
        return length * width;
    }

    @Override
    public void print() {
        System.out.println("The length is:\t" + length);
        System.out.println("The width is:\t" + width);
        System.out.println("The price of a tile is:\t" + computePrice());
    }
}

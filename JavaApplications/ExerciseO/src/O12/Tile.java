package O12;

public abstract class Tile {
    private double unitPrice;

    public Tile() {
        unitPrice = 2.0; // Im assuming you want us to copy some defaults from your notes
    }

    public Tile(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getUprice() {
        return unitPrice;
    }

    public abstract double computeArea();

    public double computePrice() {
        return (unitPrice * computeArea());
    }

    public void print() {
        System.out.println("The price of the tile is:\t"+computePrice());
    }
}

package O12;

public class TriangleTile extends Tile {
    private double base;
    private double height;

    public TriangleTile() {
        height = 0.5;
        base = 1.0;
    }

    public TriangleTile(double height, double base, double unitPrice) {
        super(unitPrice);
        this.base = base;
        this.height = height;
    }

    @Override
    public double computeArea() {
        return height * (base/2.0);
    }

    @Override
    public void print() {
        System.out.println("The height is:\t" + height);
        System.out.println("The base is:\t" + base);
        System.out.println("The price of the tile is:\t" + computePrice());
    }
}

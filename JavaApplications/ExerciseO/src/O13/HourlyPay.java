package O13;

public class HourlyPay implements Payroll {
    private double hours;
    private double payRate;

    public HourlyPay() {
        hours = 40;
        payRate = 10;
    }

    public HourlyPay(double hours, double payRate) {
        this.hours = hours;
        this.payRate = payRate;
    }

    private double getOverTime() {
        if (hours > 40)
            return 1.5 * (hours - 40) * payRate; // I think this is wrong because it means you
        else                                     // make 2.5 times the money. It should be 0.5
            return 0;                            // instead of 1.5 so that the total is 1.5
    }

    public double getGrossPay() {
        return hours * payRate + getOverTime();
    }

    public double getDeductions() {
        return TAXRATE * getGrossPay();
    }

    public void printDetails() {
        System.out.println(this);
    }

    public String toString() {
        return String.format("PAY RATE:       %s\nHOURS:          %s\n" +
                             "OVERTIME:       %s\nGROSS PAY:      %s\n" +
                             "TAX DEDUCTIONS: %s\nNET PAY:        %s\n",
                             payRate, hours, getOverTime(), getGrossPay(),
                             getDeductions(), getGrossPay()-getDeductions());
    }
}

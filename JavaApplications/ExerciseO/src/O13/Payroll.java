package O13;

public interface Payroll {
    double TAXRATE = 0.15;
    double getGrossPay();
    double getDeductions();
    void printDetails();
}

package O14;

// There was a space between ExerciseO14 originally had to remove it
public class ExerciseO14
{
    public static void main(String[  ]  args)
    {
        (new OuterClass()).outerMethod();
        (new OuterClass.NestedClass()).innerMethod2();
    }
}

class OuterClass
{
    public  void outerMethod(   )
    {
        System.out.println("outer class");
        (new NestedClass()).innerMethod1();
    }

    static  class NestedClass
    {
        public  void innerMethod1(   )
        {
            System.out.println("nested class Method1");
        }
        public  void innerMethod2(   )
        {
            System.out.println("nested class Method2");
            (new OuterClass()).outerMethod();
        }
    }   // end of class StaticNestedClass
}  // end of class OuterClass

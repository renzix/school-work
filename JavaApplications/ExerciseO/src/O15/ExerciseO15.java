package O15;

public class ExerciseO15
{
    public static void main(String[ ] args)
    {
        OuterClass oc = new OuterClass(); // 1
        oc.outerMethod(); // 1
        OuterClass.InnerClass ic = oc.new InnerClass(); // 2
    }
}

class OuterClass {
    private int outerNum;
    public OuterClass( )
    {
        outerNum = 20;
    }

    public int getOuterNum(  )
    {
        return( outerNum );
    }
    public void outerMethod(  )
    {
        System.out.println("outer class");
        // 3
        InnerClass ic = new InnerClass();
        ic.innerMethod1();
        // 4
        System.out.println(ic.innerNum);
    }

    class InnerClass
    {
        private int innerNum;
        public InnerClass( )
        {
            innerNum = 15;
        }
        public int getInnerNum( )
        {
            return( innerNum );
        }

        public void innerMethod1(  )
        {
            System.out.println("nested class 1");
        }
        public void innerMethod2(  )
        {
            System.out.println("nested class 2");
            // 5
            outerMethod();
            // 6
            System.out.println(outerNum);
        }
    }
}

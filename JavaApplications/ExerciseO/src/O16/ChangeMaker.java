package O16;

abstract class ChangeMaker {
    private int val1;
    private int val2;
    public ChangeMaker() {
        val1=0;
        val2=0;
    }

    public ChangeMaker(int val1, int val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    public int getVal1() {
        return val1;
    }
    
    public int getVal2() {
        return val2;
    }

    public void setValues(int val1, int val2) {
        this.val1 = val1;
        this.val2 = val2;
    }

    public abstract void makeChanges();

    @Override
    public String toString() {
        return String.format("\nval1=%s\nval2=%s",val1,val2);
    }

}

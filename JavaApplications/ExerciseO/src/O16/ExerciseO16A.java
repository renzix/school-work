package O16;

public class ExerciseO16A {
    public static void main(String[] args) {
        ChangeMaker obj1 = new ChangeMaker() {
                public void makeChanges(){
                    setValues(getVal1()+1,getVal2()+1);
                }
            };
        obj1.makeChanges();
        System.out.println(obj1);
        ChangeMaker obj2 = new ChangeMaker(3,4) {
                public void makeChanges(){
                    setValues(getVal1()+5,getVal2()+5);
                }
            };
        obj2.makeChanges();
        System.out.println(obj2);
    }
}

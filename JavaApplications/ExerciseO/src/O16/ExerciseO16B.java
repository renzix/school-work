package O16;

public class ExerciseO16B {
    public static void main(String[] args) {
        Expression e = new Expression() {
                public void saySomething() {
                    System.out.println("I feel good");
                }
            };
        e.saySomething();
        Expression e2 = new Expression() {
                public void saySomething() {
                    System.out.println("I do not feel well");
                }
            };
        e2.saySomething();
    }
}

package O2;

class  Demo
{
    private  double val1;						// the first data member
    private	 double val2;						// the second data member
    
    public void  setValues(double num1, double num2)
    {
        val1 = num1;
        val2 = num2;
    }
    public double getValue1() // void doesnt work in java???
    {
        return (val1);
    }
    public double getValue2()
    {
        return (val2);
    }
    public double getAverage()
    {
        return(  (val1 + val2) / 2); // Typo where val was valu
    }
    // Wouldnt it make more sense to put this into the class ExerciseO2 ??
    // The "Add to the class Demo" seems like its wrong...
    // "Add to the class Demo of Exercise O1 the static method void decrDemo( Demo obj) that subtracts 1 to the value of each of the instance variables of the object that it receives as argument."
    static void decrDemo(Demo other)
    {
        other.setValues(other.getValue1()-1, other.getValue2()-1);
    }
}

package O3;

import java.util.Scanner;

class ExerciseO3 {
    public static void main(String[] tArgs) {
        Date d = new Date();
        System.out.println(d.toString());
        Date today = new Date(24,9,2019);
        // @NOTE(DeBruno): According to the directions Date does not have
        // multiple constructors and does not have setters but it seems like
        // it should as it says to set it???
        System.out.printf("Today's day is:\t%s\n",today.getDay());
        Date dueDate = new Date();
        Scanner sc = new Scanner(System.in);
        dueDate.inputDate(sc);
        if (dueDate.isEqualTo(today))
            System.out.println("Your project is on time");
        else if (Date.isGreaterThan(dueDate, today))
            System.out.println("Your project is early");
        else
            System.out.println("Your project is late");
        Date invalid = new Date(25,4,1940);
        System.out.println("This wont execute");
    }
}

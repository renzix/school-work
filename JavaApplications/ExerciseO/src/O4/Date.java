package O4;

//////////////////////////
// NAME: Daniel DeBruno //
// DATE: 2019-09-26     //
// EDITOR: emacs        //
//////////////////////////

import java.util.Scanner;

class Date {
    private int month;
    private int day;
    private int year;

    public Date(){
        this.day = 1;
        this.month = 1;
        this.year = 1960;
        checkDate();
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
        checkDate();
    }
    
    private void checkDate() {
        
        int daysInMonth=30;
        switch (this.month) {
        case 1:
            daysInMonth++;
            break;
        case 2:
            daysInMonth=28;
            break;
        case 3:
            daysInMonth++;
            break;
        case 5:
            daysInMonth++;
            break;
        case 7:
            daysInMonth++;
            break;
        case 8:
            daysInMonth++;
            break;
        case 10:
            daysInMonth++;
            break;
        case 12:
            daysInMonth++;
            break;
        }
        if ((this.year < 1960) || (this.year > 2019)) // The program would exit early if 2016
            System.exit(1);
        else if ((this.month < 1) || (this.month >12))
            System.exit(1);
        else if((this.day<1)||(this.day>daysInMonth))
            System.exit(1);
    }

    public void inputDate(Scanner scan) {
        System.out.print("Please enter the day: ");
        this.day = scan.nextInt();
        System.out.print("Please enter the month: ");
        this.month = scan.nextInt();
        System.out.print("Please enter the year: ");
        this.year = scan.nextInt();
        checkDate();
    }

    public int getDay() {
        return this.day;
    }

    public int getMonth() {
        return this.month;
    }

    public int getYear() {
        return this.year;
    }
    
    public String toString() {
        return String.format("%s/%s/%s",month,day,year);
    }
    public boolean isEqualTo(Date obj) {
        return (this.day==obj.day)&&(this.month==obj.month)&&(this.year==obj.year);
    }

    static public boolean isGreaterThan(Date obj1, Date obj2) {
        if (obj1.year > obj2.year)
            return true;
        else if (obj1.year < obj2.year)
            return false;
        else {
            if (obj1.month > obj2.month)
                return true;
            else if (obj1.month < obj2.month)
                return false;
            else {
                if (obj1.day > obj2.day)
                    return true;
                else
                    return false;
            }
        }
    }
}

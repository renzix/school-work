package O4;

import java.util.Scanner;

public class ExerciseO4 {
    public static void main(String[] tArgs) {
        Employee def = new Employee();
        System.out.println(def.getPInfoString());
        System.out.println(def.getPayInfoString());
        Employee constructor = new Employee("John","Doe", 111111, new Date(25,10,1990), new Date(15,11,2010), 750.00);
        System.out.println(constructor.getPInfoString());
        System.out.println(constructor.getPayInfoString());
        Employee input = new Employee();
        Scanner sc = new Scanner(System.in);
        input.readPInfo(sc);
        input.readPayInfo(sc);
        System.out.println(input.getPInfoString());
        System.out.println(input.getPayInfoString());
        Employee invalid = new Employee("Date","Invalid", 111112, new Date(25,10,1965), new Date(15,11,2030), 750.00);
    }
}

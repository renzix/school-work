package O5;

import java.util.Scanner;

class Employee {
    private String firstName;
    private String lastName;
    private int id;
    private Date birthday;
    private Date dateHired;
    private double basePay;

    public Employee(){
        this.firstName = new String();
        this.lastName = new String();
        this.id = 999999;
        this.birthday = new Date();
        this.dateHired = new Date();
        this.basePay = 0.0;
    }

    // Again it doesnt say whether we need setters/getters but asks for
    // seting variables so im just gonna making a constructor.
    public Employee(String firstName, String lastName, int id,
                    Date birthday, Date dateHired, double basePay) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.id = id;
        this.birthday = birthday;
        this.dateHired = dateHired;
        this.basePay = basePay;
    }

    public void readPInfo(Scanner scan) {
        System.out.print("Please enter the first name: ");
        this.firstName = scan.nextLine();
        System.out.print("Please enter the first last: ");
        this.lastName = scan.nextLine();
        System.out.print("Please enter the id: ");
        this.id = scan.nextInt();
        System.out.print("Please enter the birthday below\n");
        this.birthday.inputDate(scan);
        System.out.print("Please enter the date hired below\n");
        this.dateHired.inputDate(scan);
    }
    public void readPayInfo(Scanner scan) {
        System.out.print("Please enter the base pay: ");
        this.basePay = scan.nextDouble();
        scan.nextLine();
    }

    public String getPInfoString() {
        return String.format("NAME: %s, %s\nID NUMBER: %s\nBIRTH DAY: %s\nDATE HIRED: %s\n",
                             lastName, firstName, id, birthday, dateHired);
    }
    
    public void setBpay(double basePay) {
        this.basePay = basePay;
    }

    public double getBpay() {
        return this.basePay;
    }

    public double getGpay() {
        return this.basePay;
    }

    // I used getGpay instead of directly accessing the variable for
    // ease of changing. In actual code i probably would just use the
    // internal variable.
    public double computeTax() {
        if (this.getGpay()>=1000)
            return 0.2;
        else if (this.getGpay() >= 800)
            return 0.18;
        else if (this.getGpay() >= 600)
            return 0.15;
        else
            return 0.1;
    }

    public String getPayInfoString() {
        return String.format("GROSS PAY: %s\nTAX DEDUCTION: %s\nNET PAY: %s\n",
                             this.getGpay(),this.computeTax(),
                             this.getGpay() - this.getGpay() * this.computeTax());
    }
}

package O5;

import java.util.Scanner;

//////////////////////////////////////////////////////////////
// Name: Daniel DeBruno                                     //
// Class: Java applications                                 //
// Editor: Emacs                                            //
// Editor Config: https://renzix.github.io/Dotfiles-Mirror/ //
//////////////////////////////////////////////////////////////

public class ExerciseO5 {
    public static void main(String[] tArgs) {
        Employee group[] = new Employee[5];
        Scanner sc = new Scanner(System.in);
        for (int i=0;i<5;i++) {
            group[i] = new Employee();
            group[i].readPInfo(sc);
            group[i].readPayInfo(sc);
        }
        for (int i=0;i<5;i++) {
            System.out.println(group[i].getPInfoString());
            System.out.println(group[i].getPayInfoString());
        }
    }
}

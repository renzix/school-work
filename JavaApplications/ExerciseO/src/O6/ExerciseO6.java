package O6;

import java.util.Scanner;

class ExerciseO6 {
    public static void main(String[] tArgs) {
        Score1 s = new Score1();
        Scanner scan = new Scanner(System.in);
        s.read(scan);
        s.print();
        // I think this is what you mean for the next part?
        // "Read the last name, first name, course name, and the 5 test scores
        // into an array and use this information to define and instantiate an
        // object of the class Score1."
        System.out.printf("Please enter your first name: ");
        String firstName = scan.nextLine();
        System.out.printf("Please enter your last name: ");
        String lastName = scan.nextLine();
        System.out.printf("Please enter your course name: ");
        String courseName = scan.nextLine();
        double[] scores = new double[5];
        for(int i=0;scores.length>i;i++) {
            System.out.printf("Please enter test %d's score:",i+1);
            scores[i] = scan.nextDouble();
        }
        Score1 s2 = new Score1(firstName, lastName, courseName, scores);
        s2.print();
    }
}

package O6;

import java.util.Scanner;

class Score1 {
    private String firstName, lastName, courseName;
    private double scores[];
    public Score1() {
        firstName = lastName = courseName = null;
        scores = new double[5];
        for(int i=0;5>i;i++)
            scores[i] = 0;
    }
    public Score1(String fname, String lname, String courseNum, double list[]) {
        firstName  = fname;
        lastName   = lname;
        courseName = courseNum;
        if (list.length != 5)
            System.exit(1);    // @NOTE(DeBruno): The idiomatic java way is to throw a
        scores = new double[5];// exception but i know you would hate that.
        for(int i=0;5>i;i++) 
            scores[i] = list[i];
    }

    double computeAverage() {
        double ret=0;
        for(int i=0;scores.length>i;i++)
            ret+=scores[i];
        return ret/scores.length;
    }

    void read(Scanner scan) {
        System.out.printf("Please enter your first name: ");
        firstName = scan.nextLine();
        System.out.printf("Please enter your last name: ");
        lastName = scan.nextLine();
        System.out.printf("Please enter your course name: ");
        courseName = scan.nextLine();
        for(int i=0;scores.length>i;i++) {
            System.out.printf("Please enter test %d's score:",i+1);
            scores[i] = scan.nextDouble();
        }
        scan.nextLine();
    }
    void print() {
        System.out.printf("First name: %s\nLast name: %s\n" +
                          "Course name: %s\n", firstName, lastName, courseName);
        for(int i=0;scores.length>i;i++)
            System.out.printf("Test score %d: %s\n",i+1,scores[i]);
        System.out.printf("Average score: %s\n",computeAverage());
    }
}

package O7;

import java.util.Scanner;

class ExerciseO7 {
    public static void main(String[] tArgs) {
        Score2 s = new Score2();
        Scanner scan = new Scanner(System.in);
        s.read(scan);
        s.print();
        System.out.printf("Please enter the number of test scores you want: ");
        int numElems = scan.nextInt();
        scan.nextLine(); // to get rid of the newline as nextInt doesnt capture it.
        Score2 s2 = new Score2(numElems);
        s2.print();
        // I think this is what you mean for the next part?
        // "Read the last name, first name, course name, and the 5 test scores
        // into an array and use this information to define and instantiate an
        // object of the class Score1."
        System.out.printf("Please enter your first name: ");
        String firstName = scan.nextLine();
        System.out.printf("Please enter your last name: ");
        String lastName = scan.nextLine();
        System.out.printf("Please enter your course name: ");
        String courseName = scan.nextLine();
        double[] scores = new double[10];
        for(int i=0;scores.length>i;i++) {
            System.out.printf("Please enter test %d's score:",i+1);
            scores[i] = scan.nextDouble();
        }
        Score2 s3 = new Score2(firstName, lastName, courseName, scores);
        s3.print();
        
    }
}

package O7;

import java.util.Scanner;

class Score2 {
    private String firstName, lastName, courseName;
    private double scores[];
    public Score2() {
        this(5);
    }
    public Score2(int numElt) {
        firstName = lastName = courseName = null;
        scores = new double[numElt];
        for(int i=0;numElt>i;i++)
            scores[i] = 0;
    }

    public Score2(String fname, String lname, String courseNum, double list[]) {
        firstName  = fname;
        lastName   = lname;
        courseName = courseNum;
        scores = new double[list.length];
        for(int i=0;list.length>i;i++) 
            scores[i] = list[i];
    }

    double computeAverage() {
        double ret=0;
        for(int i=0;scores.length>i;i++)
            ret+=scores[i];
        return ret/scores.length;
    }

    void read(Scanner scan) {
        System.out.printf("Please enter your first name: ");
        firstName = scan.nextLine();
        System.out.printf("Please enter your last name: ");
        lastName = scan.nextLine();
        System.out.printf("Please enter your course name: ");
        courseName = scan.nextLine();
        for(int i=0;scores.length>i;i++) {
            System.out.printf("Please enter test %d's score:",i+1);
            scores[i] = scan.nextDouble();
        }
        scan.nextLine();
    }
    void print() {
        System.out.printf("First name: %s\nLast name: %s\n" +
                          "Course name: %s\n", firstName, lastName, courseName);
        for(int i=0;scores.length>i;i++)
            System.out.printf("Test score %d: %s\n",i+1,scores[i]);
        System.out.printf("Average score: %s\n",computeAverage());
    }
}

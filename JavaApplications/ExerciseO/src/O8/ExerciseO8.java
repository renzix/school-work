package O8;

import java.util.Scanner;

class ExerciseO8 {
    public static void main(String[] tArgs) {
        Employee e = new Employee();
        System.out.println(e.getPInfoString());
        System.out.println(e.getPayInfoString());
        BonusEmployee b = new BonusEmployee();
        System.out.println(b.getPInfoString());
        System.out.println(b.getPayInfoString());
        Employee e2 = new Employee("John", "Doe", 111111, new Date(25,10,1991), new Date(10,5,2010), 1250.0);
        System.out.println(e2.getPInfoString());
        System.out.println(e2.getPayInfoString());
        BonusEmployee b2 = new BonusEmployee("Jobe", "Daly", 222222, new Date(5,1,1990), new Date(30,6,2011), 850.0, 250.0);
        System.out.println(b2.getPInfoString());
        System.out.println(b2.getPayInfoString());
        Employee e3 = new Employee();
        Scanner sc = new Scanner(System.in);
        e3.readPInfo(sc);
        e3.readPayInfo(sc);
        System.out.println(e3.getPInfoString());
        System.out.println(e3.getPayInfoString());
        BonusEmployee b3 = new BonusEmployee();
        b3.readPInfo(sc);
        b3.readPayInfo(sc);
        System.out.println(b3.getPInfoString());
        System.out.println(b3.getPayInfoString());
    }
}

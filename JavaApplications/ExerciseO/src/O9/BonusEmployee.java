package O9;

import java.util.Scanner;

class BonusEmployee extends Employee {
    private double bonus = 0.0;

    public BonusEmployee() {
        super();
    }
    
    public BonusEmployee(String firstName, String lastName, int id,
                         Date birthday, Date dateHired, double basePay,
                         double bonus) {
        super(firstName, lastName, id, birthday, dateHired, basePay);
        this.bonus = bonus;
    }
    
    double getBonus() {
        return bonus;
    }

    @Override
    public void readPayInfo(Scanner scan) {
        super.readPayInfo(scan);
        System.out.print("Please enter the bonus pay: ");
        bonus = scan.nextDouble();
        scan.nextLine(); // cuz double is not considered a newline
    }

    @Override
    public double getGpay() {
        return super.getGpay()+bonus;
    }

    // I really dislike having to call super.getGpay() instead of having
    // a protected member....
    @Override
    public String getPayInfoString() {
        return String.format("BASE PAY: %s\nBONUS: %s\nGROSS PAY: %s\nTAX DEDUCTION: %s\nNET PAY: %s\n",
                             super.getGpay(), bonus, getGpay(),
                             computeTax(), getGpay() - getGpay() * computeTax());
    }

    
}

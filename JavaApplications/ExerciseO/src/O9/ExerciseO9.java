package O9;

import java.util.Scanner;

class ExerciseO9 {
    public static void main(String[] tArgs) {
        Employee eemp = new Employee();
        Employee bemp1 = new BonusEmployee();
        BonusEmployee bemp2 = new BonusEmployee();
        Scanner scan = new Scanner(System.in);
        eemp.readPayInfo(scan);
        bemp1.readPayInfo(scan);
        bemp2.readPayInfo(scan);
        System.out.println("Gross pay of eemp=\t" + eemp.getGpay());
        System.out.println("Gross pay of bemp1=\t" + bemp1.getGpay());
        System.out.println("Gross pay of bemp2=\t" + bemp2.getGpay());
        System.out.println("Tax of eemp=\t" + eemp.computeTax());
        System.out.println("Tax of bemp1=\t" + bemp1.computeTax());
        System.out.println("Tax of bemp2=\t" + bemp2.computeTax());
        /* 2.
         * 
         * This is NOT valid without a explicit cast to a
         * BonusEmployee object. Note that if bemp1 was not actually a
         * BonusEmployee this would create a runtime error.
         * 
         * double bonus = ((BonusEmployee)bemp1).getBonus(); // Legal version of it
         */
    }
}

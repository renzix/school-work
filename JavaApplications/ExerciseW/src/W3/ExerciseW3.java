package W3;

public class ExerciseW3
{
    // method printNumArray
    // @NOTE(DeBruno): I added <E extneds Number> and E[] because it doesnt compile as is
    // Please fix your notes or turn E into Number and get rid of <E>
    // technically you might be able to cast E but at that point just use
    // number.
    public static <E extends Number> void printNumArray(E[] inputArray)
    {
        // DisplainputArrayements
        for( E element:inputArray )
            System.out.printf("%s ",element);
        System.out.println();
    }

    public static void main(String[] args)
    {
        //Create arrays of Integer, Double and Character
        Integer[] intArray = {1,2,3,4,5};
        Double[] doubleArray = {1.1,2.2,3.3,4.4};
        //pass a Integer Array
        System.out.println("Array integerArray contains:");
        printNumArray(intArray);
        //pass a Double Array
        System.out.println("\nArray doubleArray contains:");
        printNumArray(doubleArray);
    }
}

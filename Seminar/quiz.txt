1. Identify the problem Statement 

People who come to the interview may or may not have good character. We
need to define what good character is then decide if it is nessisary
or not.

2. Identify Alternatives

We could...

Do not speak with the boss about this and do not implement it. aka do
nothing

Speak to the boss about it and get his opinion. Ultimately we have to
make sure he is okay with this before moving forward. Making sure a
candidate will bring a positive attitude will ultimately help the
company. In this scenerio you would make a seperate process and ask
questions that could lead to someone showing their character. There is
nothing wrong about being extra sure that someone has good character.

Talk with the boss about requirements you would like the candidate to
have and implicitly judge them on it instead of asking explicit
questions. Checking into their history to seeing who they are and what
they have done. With this decision you also have the option of
directly mentioning their past in the interview.

One way to solve this is to make the application process more
strict. Only have people whom are already honest and have integrity.

Another alternative is to not tell your boss and implement it
anyway. (probably a terrible idea and cause you to get fired)

3. Evaluate and Choose Alternatives

Ultimately judging ones character is a very hard thing to do and is
immoral to get wrong or even risk getting wrong. I think checking into
someones history is more important to do BEFORE the interview rather
then during it. There is no fix "right" or "wrong" decision
either. Doing nothing or relying on the application process in my
opinion is also okay however I dont believe that is realistic and can
hurt the company. At least by checking into their history prior you
can see stuff about them that is being presented by a 3rd party who is
more likely to be immpartial. Asking about someones past is a touchy
subject and really shouldn't be apart of the interview unless its a
major red flag. It may however clear up confusion and provide
nessisary context to a given area. One problem arises which is that
people do change. In my opinion Ethically you are responsible for ALL
previous actions however we need to make sure to recognize that and
not weigh in on it too heavily.

4. Implement Decision

To implement you would have to talk to your boss about this first and
foremost. Make sure that you have his and your interests in mind when
you do this as he most likely only cares about growing the buisness
(assuming you have to convince him). Keep in mind when explaining to
him that this is not just a personal issue and that it will
fundementally help the company grow. Spending the extra time to
research a person is extremely benefitial however because it is much
harder to mask your bad actions. Speaking to him about his opinion on
how much people change should also be done.

5. Evaluate Results

While I cannot actually evaluate these results I can tell you some of
my experiences with similar cases. Good people tend to become offended
if you try to pressure them into being good. The bad people in the
world are used to pretending to be good or even think of themselves as
good. Good and bad people really is just a concept people make to
describe someone. It is more of a spectrum which you cannot see and
probably will never be able to see. The crime of judging someone
falsely is so ethically bad that it probably isn't worth the
hassle. In order to see if you think this should've been implemented
or not you can ask fellow interviewers and people that have been
interviewed on their opinion of it. You can then compare the company
to itself before the rule was established.
